function invert(obj) {
   const keys = Object.keys(obj);
   const values = Object.values(obj);
   const invert = [];
   for( let i=0; i<values.length; i++ )
   {
      invert[i]=[values[i],keys[i]];
   }   
   return invert;
}

module.exports = invert;