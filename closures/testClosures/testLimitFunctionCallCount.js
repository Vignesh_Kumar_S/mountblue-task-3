const limitFunctionCallCount = require("./../limitFunctionCallCount");

function cb() {
   console.log("Callback function invoked");
}

limitFunctionCallCount(cb, 4);
