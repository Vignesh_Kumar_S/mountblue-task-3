function counterFactory() {
   // Return an object that has two methods called `increment` and `decrement`.
   // `increment` should increment a counter variable in closure scope and return it.
   // `decrement` should decrement the counter variable and return it.
   
   var counter = 1;

   return {
      increment : function(value) {
         return (counter+=value);
      },
      decrement : function(value) {
         return (counter-=value);
      }
   };
}

module.exports = counterFactory;
